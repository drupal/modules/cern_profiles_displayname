<?php
  // $Id$  
 
 /**
 * @file: Set default values for configuration variables.
 */
  
  define('DEFAULT_LDAP_SERVER', 'xldap.cern.ch');
  define('DEFAULT_LDAP_PORT', '389');
  define('DEFAULT_LDAP_BASE_DN', 'OU=Users,OU=Organic Units,DC=cern,DC=ch' . PHP_EOL . 'OU=Externals,DC=cern,DC=ch'); 
  define('DEFAULT_LDAP_USER_BIND', ''); 
  define('DEFAULT_LDAP_PASS_BIND', ''); 
  define('DEFAULT_ATTRIBUTE', 'displayname');
  
  define('DEFAULT_SHOW_ACCOUNT_NAME', FALSE);

  define('DEFAULT_ENABLE_REDIRECT', TRUE);
  define('DEFAULT_PROFILES_SITE', 'http://profiles.web.cern.ch/');
  define('DEFAULT_UNVERIFIED_ACCOUNT_PATH', 'unverified_account');
  
  define('DEFAULT_REDIRECT_EDIT', TRUE);
  define('DEFAULT_REDIRECT_CONTACT', TRUE);
  define('DEFAULT_REDIRECT_DEVEL', TRUE);
  
